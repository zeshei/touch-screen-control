/**
 * @file serial.c
 * @brief basic serial functions
 * @author Sébastien GREGOIRE
 * @version 0.1
 * @date 2014.02.23
 *
 * Basic functions for serial connection handling.
 *
 */

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <termios.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <errno.h>

#include "serial.h"
#include "tools.h"

int serialOpen(char *device)
{
	struct termios configuration;
	memset(&configuration, 0, sizeof(configuration));
	int fd;

	if ( (fd = open(device,  O_RDWR | O_NONBLOCK) ) == -1) {
		printf("\t"COLOR_RED "[ERROR] unable to open %s : %s\n" COLOR_DEFAULT, device, strerror(errno));
		return -1;
	}

	if (tcgetattr(fd, &configuration) < 0) {
		printf("\t"COLOR_RED "[ERROR] can't get default attributes: %s\n" COLOR_DEFAULT, strerror(errno));
        return -1;
    }

	cfsetispeed(&configuration, B9600);
	cfsetospeed(&configuration, B9600);

	// 8N1
	configuration.c_cflag &= ~PARENB;					// no parity bit
	configuration.c_cflag &= ~CSTOPB;					// no stop bit
	configuration.c_cflag &= ~CSIZE;					// 8 bits data
	configuration.c_cflag |= CS8;						// 8 bits data
	configuration.c_cflag &= ~CRTSCTS;					// no flow control
	configuration.c_cflag |= CREAD | CLOCAL;			// turn on READ and ignore control lines
	
	configuration.c_iflag &= ~(IXON | IXOFF | IXANY);	// turn off s/w flow ctrl
	configuration.c_lflag &= ~(ICANON | ECHO | ECHOE | ISIG); // make raw

	configuration.c_oflag &= ~OPOST; 	// make raw

	configuration.c_cc[VMIN]  = 0;
	configuration.c_cc[VTIME] = 0;


	if (tcsetattr(fd, TCSANOW, &configuration) < 0) {
		printf("\t"COLOR_RED "[ERROR] can't set new attributes: %s\n" COLOR_DEFAULT, strerror(errno));
        return -1;
	}

	printf("\t"COLOR_GREEN "[OK] serial : \t\t" COLOR_MAGENTA "%s\n" COLOR_DEFAULT, device);

	return fd;
}

int serialWrite(int fd, const char *data)
{
	int length = strlen(data);

	if (write(fd, data, length) != length) {
		printf(COLOR_RED "[ERROR] can't send data : %s\n" COLOR_DEFAULT, strerror(errno));
		return -1;
	}
	return 0;
}


int serialWriteInt(int fd, int data)
{
	if (write(fd, &data, 1) != 1) {
		printf(COLOR_RED "[ERROR] can't send data : %s\n" COLOR_DEFAULT, strerror(errno));
		return -1;
	}
	return 0;
}


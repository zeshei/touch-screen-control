
/**
 * @file touchScreen.c
 * @brief Control USB TouchScreen
 * @author Sébastien GREGOIRE
 * @version 0.1
 * @date 2014.02.22
 *
 * USB touchscreen custom driver.
 *
 */

#include <stdio.h>
#include <string.h>

#include <linux/input.h>
#include <linux/uinput.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>

#include "touchScreen.h"
#include "tools.h"

struct input_event event;


int openMouseDevice(char *device)
{
	int mouseDescriptor = open(device, O_RDONLY);
	if (mouseDescriptor < 0)
	{
		printf("\t"COLOR_RED "[ERROR] unable to open %s : %s\n" COLOR_DEFAULT, device, strerror(errno));
		return -1;
	}

    printf("\t"COLOR_GREEN "[OK] touchscreen : \t" COLOR_MAGENTA "%s\n" COLOR_DEFAULT, device);

	return mouseDescriptor;
}


int simpleRead(int fd)
{
    memset(&event, 0, sizeof(event));
    if (read(fd, &event, sizeof(struct input_event)) < 0) {
        printf(COLOR_RED "[ERROR] can't read : %s\n" COLOR_DEFAULT, strerror(errno));
        return -1;
    }

    if (event.type == EV_ABS)
    {
        printf("%d | code %d : %d\n", event.type, event.code, event.value);    
    }

    return 0;
}

Position getTouchScreenDirection(int fd)
{
    int x = 0, y = 0;
    
    do {
        memset(&event, 0, sizeof(event));
        if (read(fd, &event, sizeof(struct input_event)) < 0) {
            printf(COLOR_RED "[ERROR] can't read : %s\n" COLOR_DEFAULT, strerror(errno));
            return -1;
        }

        if (event.type == EV_ABS)
        {
            if (event.code == ABS_X)
            {
                x = event.value;
            }
            if (event.code == ABS_Y)
            {
                y = event.value;
            }
        }

    } while (x == 0 || y == 0);
    
    if (x < LOWER_BOUND)
    {
        if (y < LOWER_BOUND) {
            return BOTTOM_LEFT;
        }
        else if (y > UPPER_BOUND) {
            return TOP_LEFT;
        }
        else {
            return LEFT;
        }
    }
    else if (x > UPPER_BOUND) {
        if (y < LOWER_BOUND) {
            return BOTTOM_RIGHT;
        }
        else if (y > UPPER_BOUND) {
            return TOP_RIGHT;
        }
        else {
            return RIGHT;
        }
    }
    else if (y < LOWER_BOUND) {
        return BOTTOM;
    }
    else if (y > UPPER_BOUND) {
        return TOP;
    }

    return CENTER;
}


Direction getMouseDirection(int fd)
{
    int i = 0;
    int check[SENSIBILITY] = {0};

    // get directions
    for (i = 0; i<SENSIBILITY;)
    {
        memset(&event, 0, sizeof(event));
        if (read(fd, &event, sizeof(struct input_event)) < 0) {
            printf(COLOR_RED "[ERROR] can't read : %s\n" COLOR_DEFAULT, strerror(errno));
            return -1;
        }

        if (event.type == EV_REL)
        {
            if (event.code == REL_X) 
            {
                if (event.value > 0) 
                {
                    check[i] = EAST;
                }
                else
                {
                    check[i] = WEST;
                }
            }
            else if (event.code == REL_Y)
            {
                if (event.value > 0)
                {
                    check[i] = SOUTH;
                }
                else
                {
                    check[i] = NORTH;
                }
            }

            i++;    // loop increment
        }
    }

    // sum directions
    int max = 0;
    int counts[4] = {0};
    for (i = 0; i < SENSIBILITY; i++)
    {
        switch (check[i])
        {
            case NORTH  : counts[NORTH]++;  break;
            case EAST   : counts[EAST]++;   break;
            case SOUTH  : counts[SOUTH]++;  break;
            case WEST   : counts[WEST]++;   break;
            default : break;
        }
    }

    // get more important direction
    max = NORTH;
    for (i = 1; i < 4; i++)
    {
        if (counts[max] < counts[i])
        {
            max = i;
        }
    }

    return max;
}

int checkFunction(char *device)
{
    int fd = openMouseDevice(device);
    if (fd < 0) {
        return -1;
    }

	while (1)
    {
        printf("%s\n", positionToString(getTouchScreenDirection(fd)));
        
        usleep(10000);
    }

    return 0;
}

char * positionToString(int position)
{
    switch(position)
    {
        case TOP_LEFT :     return("TOP_LEFT");       break;
        case TOP :          return("TOP");            break;
        case TOP_RIGHT :    return("TOP_RIGHT");      break;
        case RIGHT :        return("RIGHT");          break;
        case BOTTOM_RIGHT : return("BOTTOM_RIGHT");   break;
        case BOTTOM :       return("BOTTOM");         break;
        case BOTTOM_LEFT :  return("BOTTOM_LEFT");    break;
        case LEFT :         return("LEFT");           break;
        case CENTER :       return("CENTER");         break;
        default : break;
    }

    return NULL;
}

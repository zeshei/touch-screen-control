
enum Positions {TOP_LEFT=0, TOP, TOP_RIGHT, RIGHT, BOTTOM_RIGHT, BOTTOM, BOTTOM_LEFT, LEFT, CENTER};

int rcvData = 0;

void setup() {

  /*
   * see : http://arduino.cc/en/Reference/PortManipulation
   */
  
  // PORTD maps to Arduino digital pins 0 to 7
  // Data Direction Register D (DDRD)
  DDRD = B11111111; // all in output
  
  Serial.begin(9600);
  Serial.flush();
}

void loop()
{
  if (Serial.available())
  {
    rcvData = Serial.read();
    //Serial.println(rcvData);
    
    switch (rcvData)
    {
      /*
      case TOP_LEFT :
        PORTD = (PORTD & 1) ? B00000000 : B00000001;
        break;
      case TOP :
        PORTD = ((PORTD >> 1) & 1) ? B00000000 : B00000010;
    	  break;
      case TOP_RIGHT :        
        PORTD = ((PORTD >> 2) & 1) ? B00000000 : B00000100;
    	  break;
      case RIGHT :
    	  PORTD = ((PORTD >> 3) & 1) ? B00000000 : B00001000;
    	  break;
      case BOTTOM_RIGHT :
    	  PORTD = ((PORTD >> 4) & 1) ? B00000000 : B00010000;
    	  break;
      case BOTTOM :
    	  PORTD = ((PORTD >> 5) & 1) ? B00000000 : B00100000;
    	  break;
      case BOTTOM_LEFT :
    	  PORTD = ((PORTD >> 6) & 1) ? B00000000 : B01000000;
    	  break;
      case LEFT :
    	  PORTD = ((PORTD >> 7) & 1) ? B00000000 : B10000000;
    	  break;
        default : break;
      */
      case TOP_LEFT :
        PORTD = B00000001;
        break;
      case TOP :
        PORTD = B00000010;
        break;
      case TOP_RIGHT :        
        PORTD = B00000100;
        break;
      case RIGHT :
        PORTD = B00001000;
        break;
      case BOTTOM_RIGHT :
        PORTD = B00010000;
        break;
      case BOTTOM :
        PORTD = B00100000;
        break;
      case BOTTOM_LEFT :
        PORTD = B01000000;
        break;
      case LEFT :
        PORTD = B10000000;
        break;
      case CENTER : 
        PORTD = B00000000;
        break;
      default : 
        PORTD = B00000000;
        break;
    }
  }
}

#Readme

---

## compilation

	gcc -o TouchScreenControl *.c -Wall

## execution
	
Usage:
	
	$PROGRAM -t touch_screen_device -s serial_device

Example:

	~#./TouchScreenControl -t /dev/input/event2 -s /dev/ttyACM0


## Hardware
	  _________
	 |         [2]-------LedO
	 |         [3]-------Led1
	 |         [4]-------Led2
	 | Arduino [5]-------Led3
	 |         [6]-------Led4
	 |         [7]-------Led5
	 |         [8]-------Led6
	 |_ _ _____[9]-------Led7
	   |_|                         ________________
	    |                         |_|_ _ _ _ _ _ |_|
	 USB|    _____________________|                |
	    |   |   USB               | |            | |
	    |   |                     |  touch screen  |
	 +-|_|-|_|----+               | |            | |
	 |            |               |_ _ _ _ _ _ _ _ |
	 |            |               |_|____________|_|
	 |     PC     |
	 |            |
	 +------------+

/**
 * @file main_touchScreenControl.c
 * @brief Send acquired touchscreen data on serial
 * @author Sébastien GREGOIRE
 * @version 0.1
 * @date 2014.02.23
 *
 * Get the touched position in the touch screen and send acquired data on a serial connection.
 *
 */

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>

#include "main_touchScreenControl.h"
#include "touchScreen.h"
#include "serial.h"
#include "tools.h"

int touchScreenFd = 0;
int serialFd = 0;

char *touchScreenDevice = NULL;
char *serialDevice = NULL;

int getParameters(int argc, char *argv[])
{
	int opt;
	while ((opt = getopt(argc, argv, "t:s:")) != -1) {
		switch (opt) {
			case 't':
				touchScreenDevice = optarg;
				break;
			case 's':
				serialDevice = optarg;
				break;
			break;
			default:
				fprintf(stderr, "\nUsage: %s -t touchScreenDevice -s serialDevice\n\n", argv[0]);
				return -1;
		}
   }
   if (touchScreenDevice == NULL || serialDevice == NULL) {
		fprintf(stderr, "\nUsage: %s -t touchScreenDevice -s serialDevice\n\n", argv[0]);
		return -1;
   }

   return 0;
}

int openDevices()
{
	// open touch screen
	if ((touchScreenFd = openMouseDevice(touchScreenDevice)) < 0) {
		return -1;
	}

	// open serial
	if ((serialFd = serialOpen(serialDevice)) < 0) {
		return -1;
	}

	return 0;
}

int main(int argc, char *argv[])
{
	
	int position = 0;

	puts("\nInitialization\n");

	if (getParameters(argc, argv) < 0) {
		exit(EXIT_FAILURE);
	}

	if (openDevices() < 0) {
		exit(EXIT_FAILURE);
	}

	puts("\nProgram is running" STYLE_BLINK "." STYLE_DEFAULT "\n");

	// main loop
	while (1)
	{
		position = getTouchScreenDirection(touchScreenFd);
		if (position >= 0)
		{
			printf(COLOR_CYAN "%s\n" COLOR_DEFAULT, positionToString(position));
			serialWriteInt(serialFd, position);
		}

		usleep(UTIME_BETWEEN_TOUCH);
	}
	return 0;
}

/**
 * @file serial.h
 * @brief basic serial functions
 * @author Sébastien GREGOIRE
 * @version 0.1
 * @date 2014.02.23
 *
 * Basic functions for serial connection handling.
 *
 */


#ifndef SERIAL 
#define SERIAL

/**
 * @brief open a serial connection
 * 
 * @param device device location
 * @return file descriptor if success, -1 if error
 */
int serialOpen(char *device);

/**
 * @brief write data on a serial connection
 * 
 * @param fd file descriptor
 * @param data data to write
 * @return 0 if success, -1 if error
 */
int serialWrite(int fd, const char *data);


/**
 * @fn int serialWriteInt(int fd, int data)
 * @brief write an integer on serial
 * 
 * @param fd file descriptor
 * @param data int to send
 * 
 * @return 0 if success, -1 if error
 */
int serialWriteInt(int fd, int data);

#endif

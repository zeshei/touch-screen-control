/**
 * @file tools.h
 * @brief Colors and styles for tracing
 * @author Sébastien GREGOIRE
 * @version 0.1
 * @date 2014.02.27
 *
 * Tools for custom tracing
 *
 */


#ifndef TOOLS
#define TOOLS


/**
 * red color for traces
 */
#define COLOR_RED "\x1b[31m"

/**
 * green color for traces
 */
#define COLOR_GREEN "\x1b[32m"

/**
 * blue color for traces
 */
#define COLOR_BLUE "\x1b[34m"

/**
 * magenta color for traces
 */
#define COLOR_MAGENTA "\x1b[35m"

/**
 * cyan color for traces
 */
#define COLOR_CYAN "\x1b[36m"

/**
 * set default color in traces
 */
#define COLOR_DEFAULT "\x1b[0m"



/**
 * blinking style
 */
#define STYLE_BLINK "\x1b[5m"

/**
 * bold style
 */
#define STYLE_BOLD "\x1b[1m"

/**
 * default style
 */
#define STYLE_DEFAULT "\x1b[0m"


#endif

/**
 * @file touchScreen.h
 * @brief Control USB TouchScreen
 * @author Sébastien GREGOIRE
 * @version 0.1
 * @date 2014.02.22
 *
 * USB touchscreen custom driver.
 *
 */

#ifndef TOUCH_SCREEN_CONTROL 
#define  TOUCH_SCREEN_CONTROL

/**
 * (for mouse acquisition) less is more sensitive
 */
#define SENSIBILITY 50

/**
 * Absolute value of the screen bottom and left areas
 */
#define LOWER_BOUND 600

/**
 * Abosolute value of the screen top and right areas
 */
#define UPPER_BOUND 1280

/**
 * @enum Position
 * @brief Positions for touch screen
 */
typedef enum {TOP_LEFT=0, TOP, TOP_RIGHT, RIGHT, BOTTOM_RIGHT, BOTTOM, BOTTOM_LEFT, LEFT, CENTER} Position;

/**
 * @enum Direction
 * @brief Direction for mouse movements
 */
typedef enum {NORTH=0, EAST, SOUTH, WEST} Direction;


/**
 * @fn int openMouseDevice(char *device)
 * @brief open the touchScreen device
 * @param device character string to the device
 * @return file descriptor if success, -1 otherwise
 * 
 * Generally devices are located on "/dev/input/eventX"
 * 
 */
int openMouseDevice(char *device);

/**
 * @fn void simpleRead(int fd)
 * @brief does a simple read of the data transmitted by the device
 * @param fd file descriptor
 * @return 0 if sucess, -1 if error
 */
int simpleRead(int fd);

/**
 * @fn Position getTouchScreenDirection(int fd)
 * @brief get the touched Position
 * @param fd file descriptor
 * @return clicked position. Position if on the screen sides, -1 otherwise.
 */
Position getTouchScreenDirection(int fd);

/**
 * @fn Direction getMouseDirection(int fd)
 * @brief get a mouse position
 * @param fd file descriptor
 * @return Direction of the mouse, -1 if reading error
 */
Direction getMouseDirection(int fd);

/**
 * @fn int checkFunction(char *device)
 * @brief main check function
 * @param device device location
 * @return -1 if error
 */
int checkFunction(char *device);


/**
 * @fn char * positionToString(int position)
 * @brief print a String describing the Position
 * @param position Position
 * @return string of the Position
 */
char * positionToString(int position);


#endif

/**
 * @file main_touchScreenControl.h
 * @brief Send acquired touchscreen data on serial
 * @author Sébastien GREGOIRE
 * @version 0.1
 * @date 2014.02.23
 *
 * Get the touched position in the touch screen and send acquired data on a serial connection.
 *
 */

#ifndef MAIN_TOUCH_SCREEN_CONTROL
#define MAIN_TOUCH_SCREEN_CONTROL


/**
 * Minimum time in microsecond between to touches
 */
#define UTIME_BETWEEN_TOUCH 500000


/**
 * @fn int main(int argc, char *argv[])
 * @brief main function
 * @param argc argc
 * @param argv argv
 * @return execution status code
 */
int main(int argc, char *argv[]);

/**
 * @fn int getParameters(int argc, char *argv[])
 * @brief get the devices locations
 * @param argc argc
 * @param argv argv
 * @return 0 if success, -1 if error
 */
int getParameters(int argc, char *argv[]);

/**
 * @fn int openDevices()
 * @brief open touch screen and serial
 * @return 0 if success, -1 if error
 */
int openDevices();

#endif